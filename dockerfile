# Script by Ken Yeh (@leafyeh7)
# R Shiny Server Docker Image with mapping and dashboard packages

# Make sure rocker/shiny image container is update to date 
FROM rocker/shiny:4.0.4
# [Install dependencies and Download and install shiny server]
# RUN apt-get update && apt-get install -y -t unstable \
RUN apt-get update && apt-get install -y \
    libssl-dev && \
    # [Unused R packages]
    # R -e "install.packages(c('devtools','feather', 'leaflet.esri', 'geojsonio', 'rgdal', 'maptools'), repos='https://cran.rstudio.com/')" && \
    R -e "install.packages('devtools', repos='https://cran.rstudio.com/')" &&\
    # [Force R Leaflet 1.1.0.  Required for Leaflet.MapboxVectorTile]
    # R -e "devtools::install_version('leaflet', version = '1.1.0', repos = 'http://cran.us.r-project.org')" &&\
    # R -e "install.packages(c('shinydashboard','flexdashboard','httr','DT','jsonlite'), repos='https://cran.rstudio.com/')" &&\
    R -e "install.packages(c('shinydashboard','flexdashboard','httr','jsonlite','leaflet','shinyWidgets', 'shinyjs', 'sunburstR', 'tidyr', 'DT'), repos='https://cran.rstudio.com/')" &&\
    # [ Upgrade shiny packages ]
    # R -e "update.packages('shiny', repos='https://cran.rstudio.com/', ask = FALSE)" &&\
    # [Install tinytex for rmarkdown(*.rmd) .pdf rendering]
    wget -qO- \
    "https://github.com/yihui/tinytex/raw/master/tools/install-unx.sh" | \
    sh -s - --admin --no-path \
    && mv ~/.TinyTeX /opt/TinyTeX \
    && /opt/TinyTeX/bin/*/tlmgr path add \
    # [Added multirow for kableExtra in styling table in .rmd file]
    && tlmgr install metafont mfware inconsolata tex ae parskip listings multirow \
    && tlmgr path add \
    && Rscript -e "source('https://install-github.me/yihui/tinytex'); tinytex::r_texmf()" \
    && chown -R root:staff /opt/TinyTeX \
    && chmod -R g+w /opt/TinyTeX \
    && chmod -R g+wx /opt/TinyTeX/bin



# [Build/test commands]

# [A] Build image (navigate to dockerfile folder):
# docker build ./ -t {ImageName}:{ImageTag}

# [B] Run image for local testing:
# (only folder under current users can be accessed by virtual environment, otherwise grant drive access through docker desktop app, beware of firewall)
# (https://stackoverflow.com/questions/42203488/settings-to-windows-firewall-to-allow-docker-for-windows-to-share-drive/43904051)
# (run in PowerShell "Set-NetConnectionProfile -interfacealias "vEthernet (DockerNAT)" -NetworkCategory Private")
# (example in Windows)
# docker run -d -p 80:3838 -v /c/Users/kenyeh/Documents/Kitematic/shiny/:/srv/shiny-server/  -v /c/Users/kenyeh/Documents/Kitematic/shinylog/:/var/log/shiny-server/  leafyeh7/docker-shinydashboard
# docker run -d -p 80:3838 -v /d/docker/shiny/:/srv/shiny-server/  -v /d/docker/shinylog/:/var/log/shiny-server/ shinytest11

# [C] Push to docker hub:
# 1. docker login
# 2. docker tag {ImageName} {RepositoryUserName/RepositoryImageName}:{Tag}
# 3. docker push {RepositoryUserName/RepositoryImageName}:latest
